package br.com.dbccompany.hackathon.Entity;

import java.util.List;

public class Venda {
    private final Integer ID = 003;
    private String idVenda;
    private List<Item> itens;
    private String vendedor;
    private Double valor;

    public Venda(String idVenda, List<Item> itens, String vendedor) {
        this.idVenda = idVenda;
        this.itens = itens;
        this.vendedor = vendedor;
        this.valor = calcularValor( itens );
    }

    private Double calcularValor ( List<Item> itens ) {
        Double retorno = 0.0;
        for( Item item : itens ) {
            retorno += ( item.getPreco() * item.getQuantidade() );
        }
        return retorno;
    }

    public Integer getID() {
        return ID;
    }

    public String getIdVenda() {
        return idVenda;
    }

    public void setIdVenda(String idVenda) {
        this.idVenda = idVenda;
    }

    public List<Item> getItens() {
        return itens;
    }

    public void setItens(List<Item> itens) {
        this.itens = itens;
    }

    public String getVendedor() {
        return vendedor;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
