package br.com.dbccompany.hackathon.Processor;

import br.com.dbccompany.hackathon.Entity.*;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component(value = "processador")
public class Processor implements ItemProcessor<Dados, DadosWriter> {

    private List<Venda> vendas = new ArrayList<>();
    private List<Vendedor> vendedores = new ArrayList<>();
    private List<Cliente> clientes = new ArrayList<>();

    DadosProcessados dados = new DadosProcessados();

    @Override
    public DadosWriter process(Dados dado) throws Exception {
        if ( dado.getId().equals("001") ) {
            this.montarVendedor( dado );
        }else if ( dado.getId().equals("002")){
            this.montarCliente( dado );
        }else if ( dado.getId().equals("003")){
            this.montarVenda( dado );
        }

        this.montarDadosProcessados();

        return dados.retornarBonito();
    }

    private void montarVendedor( Dados dado ){
        String cpfVendedor = dado.getPrimeiraInfo();
        String nomeVendedor = dado.getSegundaInfo();
        Double salarioVendedor = Double.parseDouble(dado.getTerceiraInfo());
        Vendedor vendedor = new Vendedor(cpfVendedor, nomeVendedor, salarioVendedor);
        if ( !vendedorExist( vendedor ) ){
            vendedores.add( vendedor );
        }
    }

    private void montarCliente( Dados dado ){
        String cnpj = dado.getPrimeiraInfo();
        String nome = dado.getSegundaInfo();
        String area = dado.getTerceiraInfo();
        Cliente cliente  = new Cliente( cnpj, nome, area );
        if ( !clienteExist( cliente ) ){
            clientes.add( cliente );
        }
    }

    private void montarVenda( Dados dado ){
        String idVenda = dado.getPrimeiraInfo();
        List<Item> itensVenda = construirListaItens(dado.getSegundaInfo());
        String vendedorVenda = dado.getTerceiraInfo();
        vendas.add( new Venda(idVenda, itensVenda, vendedorVenda));
    }

    private void montarDadosProcessados(){
        dados.setQuantidadeDeClientes(this.clientes.size());
        dados.setQuantidadeDeVendedores(this.vendedores.size());
        dados.setIdVendaMaisCara(procurarVendaMaisCara());
        dados.setPiorVendedor(piorVendedor());
    }

    private List<Item> construirListaItens (String stringLista ) {
        List<Item> listaItens = new ArrayList<>();
        stringLista = stringLista.replace("[", "");
        stringLista = stringLista.replace("]", "");
        String[] stringDeInfoDeObjetos = stringLista.split(",");
        for( String string : stringDeInfoDeObjetos ) {
            String[] stringsParaConstrucaoDeItem = string.split("-");
            Integer idItem = Integer.parseInt(stringsParaConstrucaoDeItem[0]);
            Integer quantidadeItem = Integer.parseInt(stringsParaConstrucaoDeItem[1]);
            Double precoItem = Double.parseDouble(stringsParaConstrucaoDeItem[2]);
            listaItens.add(new Item(idItem, quantidadeItem, precoItem));
        }
        return listaItens;
    }

    private String procurarVendaMaisCara() {
        Collections.sort(vendas, Comparator
                .<Venda>comparingDouble(venda1 -> venda1.getValor())
                .thenComparing(venda2 -> venda2.getValor()));
        if ( vendas.size() == 0 ) {
            return null;
        }
        return vendas.get( vendas.size() - 1 ).getIdVenda();
    }

    private String piorVendedor() {
        String piorV = "";
        Integer piorVIndice = null;
        for ( int i = 0; i < vendas.size(); i++){
            if ( i == 0 ){
                piorV = vendas.get(i).getVendedor();
                piorVIndice = i;
            }else{
                if ( vendas.get(i).getValor() < vendas.get(piorVIndice).getValor() ) {
                    piorV = vendas.get(i).getVendedor();
                    piorVIndice = i;
                }
            }
        }
        return piorV;
    }

    private Boolean vendedorExist( Vendedor vendedor ){
        if ( this.vendedores.isEmpty() ){
            return false;
        }
        List<Vendedor> list = vendedores.stream().filter( vendedorX -> (vendedorX.getCpf().equals(vendedor.getCpf())) ).collect(Collectors.toList());
        if ( list.isEmpty() ){
            return false;
        }else{
            return true;
        }
    }

    private Boolean clienteExist( Cliente cliente ){
        if ( this.clientes.isEmpty() ){
            return false;
        }
        List<Cliente> list = clientes.stream().filter( vendedorX -> (vendedorX.getCnpj().equals(cliente.getCnpj())) ).collect(Collectors.toList());
        if ( list.isEmpty() ){
            return false;
        }else{
            return true;
        }
    }

}
