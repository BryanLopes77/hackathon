package br.com.dbccompany.hackathon.Entity;

import java.sql.Array;
import java.util.ArrayList;

public class DadosProcessados {

    private Integer quantidadeDeClientes;
    private Integer quantidadeDeVendedores;
    private String idVendaMaisCara;
    private String piorVendedor;

    public DadosProcessados() {
    }

    public DadosProcessados(Integer quantidadeDeClientes, Integer quantidadeDeVendedores, String idVendaMaisCara, String piorVendedor) {
        this.quantidadeDeClientes = quantidadeDeClientes;
        this.quantidadeDeVendedores = quantidadeDeVendedores;
        this.idVendaMaisCara = idVendaMaisCara;
        this.piorVendedor = piorVendedor;
    }

    public Integer getQuantidadeDeClientes() {
        return quantidadeDeClientes;
    }

    public void setQuantidadeDeClientes(Integer quantidadeDeClientes) {
        this.quantidadeDeClientes = quantidadeDeClientes;
    }

    public Integer getQuantidadeDeVendedores() {
        return quantidadeDeVendedores;
    }

    public void setQuantidadeDeVendedores(Integer quantidadeDeVendedores) {
        this.quantidadeDeVendedores = quantidadeDeVendedores;
    }

    public String getIdVendaMaisCara() {
        return idVendaMaisCara;
    }

    public void setIdVendaMaisCara(String idVendaMaisCara) {
        this.idVendaMaisCara = idVendaMaisCara;
    }

    public String getPiorVendedor() {
        return piorVendedor;
    }

    public void setPiorVendedor(String piorVendedor) {
        this.piorVendedor = piorVendedor;
    }

    public DadosWriter retornarBonito(){
        DadosWriter dadosWriter = new DadosWriter();
        dadosWriter.setQuantidadeDeClientes("Quantidade de Clientes: " + this.quantidadeDeClientes );
        dadosWriter.setQuantidadeDeVendedores("Quantidade de vendedores: " + this.quantidadeDeVendedores );
        dadosWriter.setIdVendaMaisCara("ID venda mais cara: " + this.idVendaMaisCara );
        dadosWriter.setPiorVendedor("Pior vendedor: " + this.piorVendedor );
        return dadosWriter;
    }
}
