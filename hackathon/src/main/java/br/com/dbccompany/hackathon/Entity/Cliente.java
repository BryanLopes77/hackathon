package br.com.dbccompany.hackathon.Entity;

public class Cliente {
    private final Integer ID = 002;
    private String cnpj;
    private String nome;
    private String area;

    public Cliente(String cnpj, String nome, String area) {
        this.cnpj = cnpj;
        this.nome = nome;
        this.area = area;
    }

    public Integer getID() {
        return ID;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
}
