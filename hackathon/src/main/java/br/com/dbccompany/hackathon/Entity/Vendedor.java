package br.com.dbccompany.hackathon.Entity;

public class Vendedor {
    private final Integer ID = 001;
    private String cpf;
    private String nome;
    private Double salario;
    private Double valorVendas = 0.0;

    public Vendedor(String cpf, String nome, Double salario) {
        this.cpf = cpf;
        this.nome = nome;
        this.salario = salario;
    }

    public Integer getID() {
        return ID;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getSalario() {
        return salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public Double getValorVendas() {
        return valorVendas;
    }

    public void setValorVendas(Double valorVendas) {
        this.valorVendas = valorVendas;
    }
}
