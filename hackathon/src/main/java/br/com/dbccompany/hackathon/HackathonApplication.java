package br.com.dbccompany.hackathon;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.nio.file.*;

@SpringBootApplication
@EnableScheduling
public class HackathonApplication {

	@Autowired
	JobLauncher jobLauncher;

	@Autowired
	Job job;

	public void perform() throws Exception{
		JobParameters params = new JobParametersBuilder()
				.addString("JobID", String.valueOf(System.currentTimeMillis()))
				.toJobParameters();

		jobLauncher.run(job, params);
	}

	public static void main(String[] args) {
		SpringApplication.run(HackathonApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class	)
	public void run() throws Exception {

		WatchService watchService = FileSystems.getDefault().newWatchService();

		Path path = Paths.get(System.getProperty("user.home") + "/Documents/hackathon/hackathon/hackathon/src/main/resources/data");

		path.register(
				watchService,
				StandardWatchEventKinds.ENTRY_CREATE,
				StandardWatchEventKinds.ENTRY_MODIFY,
				StandardWatchEventKinds.ENTRY_DELETE,
				StandardWatchEventKinds.OVERFLOW);


		WatchKey key;
		while ((key = watchService.take()) != null) {
			for (WatchEvent<?> event : key.pollEvents()) {

				System.out.println("Event kind:" + event.kind()+ ". File affected: " + event.context() + ".");

				if(event.kind().name().equals("ENTRY_CREATE")) {
					perform();
				}

			}
			key.reset();
		}


	}

}
